/**
 * Compile .scss source to inline .html.twig css file. 
 */

const gulp = require('gulp');
const sass = require('gulp-sass');
const stripCssComments = require('gulp-strip-css-comments');
const cleanDir = require('gulp-clean-dir');
const rename = require("gulp-rename");

gulp.task('default', function () {
    gulp.src('./scss/*.scss')
        .pipe(sass({outputStyle: 'compressed', follow: true}).on('error', sass.logError))
        .pipe(stripCssComments())
        .pipe(cleanDir('./templates/amp-css/'))
        .pipe(rename('amp-custom-styles.html.twig'))
        .pipe(gulp.dest('./templates/amp-css/'));
});

gulp.task('watch', function () {
    gulp.watch('scss/**/*.scss', ['default']);
});